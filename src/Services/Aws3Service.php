<?php
namespace App\Services;

use Aws\S3\S3Client;

class Aws3Service
{
    /**
     * @var S3Client
     */
    private $client;

    /**
     * @var string
     */
    private $bucket;

    /**
     * @param string $bucket
     * @param array  $s3arguments
     */
    public function __construct($bucket, array $s3arguments)
    {
        $this->setBucket($bucket);
        $this->setClient(new S3Client($s3arguments));
    }

    public function upload( $fileName, $content)
    {
        try {
            return $this->getClient()->upload($this->getBucket(), $fileName->getClientOriginalName(), $content);
        } catch (\Exception $exception) {
            var_dump($exception->getMessage());
        }
    }

    public function uploadFile($fileName) {
        return $this->upload($fileName, file_get_contents($fileName));
    }

    public function getListBucket()
    {
        return $this->getClient()->getIterator('ListObjects', array('Bucket' => $this->getBucket()), array('limit'  => 1));
    }

    public function delete($key)
    {
        return $this->getClient()->deleteObject(['Bucket' => $this->getBucket(), 'Key' => $key]);
    }


    /**
     * Getter of client
     *
     * @return S3Client
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * Setter of client
     *
     * @param S3Client $client
     *
     * @return $this
     */
    private function setClient(S3Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Getter of bucket
     *
     * @return string
     */
    protected function getBucket()
    {
        return $this->bucket;
    }

    /**
     * Setter of bucket
     *
     * @param string $bucket
     *
     * @return $this
     */
    private function setBucket($bucket)
    {
        $this->bucket = $bucket;

        return $this;
    }
}