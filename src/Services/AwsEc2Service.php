<?php
namespace App\Services;

use Aws\Ec2\Ec2Client;
use Aws\S3\S3Client;

class AwsEc2Service
{
    /**
     * @var Ec2Client
     */
    private $client;

    /**
     * @var string
     */
    private $bucket;

    /**
     * @param string $bucket
     * @param array  $s3arguments
     */
    public function __construct($bucket, array $s3arguments)
    {
        $this->setBucket($bucket);
        $this->setClient(new Ec2Client($s3arguments));
    }

    /**
     * Getter of client
     *
     * @return S3Client
     */
    protected function getClient()
    {
        return $this->client;
    }

    /**
     * Setter of client
     *
     * @param Ec2Client $client
     *
     * @return $this
     */
    private function setClient(Ec2Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Getter of bucket
     *
     * @return string
     */
    protected function getBucket()
    {
        return $this->bucket;
    }

    /**
     * Setter of bucket
     *
     * @param string $bucket
     *
     * @return $this
     */
    private function setBucket($bucket)
    {
        $this->bucket = $bucket;

        return $this;
    }

    public function instanceList()
    {
        return $this->getClient()->describeInstances();
    }

    public function newInstance()
    {
        $this->getClient()->runInstances(array(
            'ImageId'        => 'ami-03b755af568109dc3',
            'MinCount'       => 1,
            'MaxCount'       => 1,
            'InstanceType'   => 't2.micro',
            'KeyName'        => 'mds',
        ));
    }
}