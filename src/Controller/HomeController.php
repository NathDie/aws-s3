<?php

namespace App\Controller;

use App\Form\AwsType;
use App\Services\AwsEc2Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Aws3Service;

class HomeController extends AbstractController
{
    protected Aws3Service $aws3Service;
    protected AwsEc2Service $awsEc2Service;

    public function __construct(Aws3Service $aws3Service, AwsEc2Service $awsEc2Service)
    {
        $this->aws3Service = $aws3Service;
        $this->awsEc2Service = $awsEc2Service;
    }

    #[Route('/', name: 'app_home')]
    public function uploadInBucket(Request $request)
    {
        $listes = $this->aws3Service->getListBucket();
        $form = $this->createForm(AwsType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $this->aws3Service->uploadFile($data['upload']);
        }

        return $this->render('home/index.html.twig', [
            'listes' => $listes,
            'form' => $form->createView()
        ]);
    }

    #[Route('/delete', name: 'app_home_delete')]
    public function deleteInBucket(Request $request)
    {
        try {
            $file = $request->query->get('file');
            $this->aws3Service->delete($file);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        return $this->redirectToRoute('app_home');
    }

    #[Route('/ec2', name: 'app_home_ec2')]
    public function indexEc2(Request $request)
    {
        $instances = $this->awsEc2Service->instanceList();

        return $this->render('home/ec2.html.twig', [
            'instances' => $instances,
        ]);

    }

    #[Route('/ec2/new-instance', name: 'app_home_ec2_new')]
    public function newInstance(Request $request)
    {
        try {
            $this->awsEc2Service->newInstance();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }

        return $this->redirectToRoute('app_home_ec2');
    }
}
