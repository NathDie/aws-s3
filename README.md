# AWS S3 Webapp

This project is a simple AWS S3 web application


## Project

[![php version][php-image]][php-url]
[![symfony version][symfony-image]][symfony-url]

[php-image]: https://img.shields.io/badge/php-8.1-blue
[php-url]: https://www.php.net/

[symfony-image]: https://img.shields.io/badge/symfony-6.1.*-green
[symfony-url]: https://symfony.com/

## Authors

- [@NathDie](https://www.gitlab.com/NathDie)


## Deployment

Install a project

```bash
  composer install
```

Install a webpack

```bash
  yarn install
  yarn dev dev
```


## Variables setting

create .env.local file in the project with your custom variable value

```bash
AWS_BUCKET_NAME=
AWS_KEY=
AWS_SECRET_KEY=
```